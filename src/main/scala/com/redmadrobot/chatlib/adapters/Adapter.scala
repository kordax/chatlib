package com.redmadrobot.chatlib.adapters

/**
  * Basic Adapter trait
  */
trait Adapter {
  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  def getConnectionString: String
}
