package com.redmadrobot.chatlib.adapters.db

import com.redmadrobot.chatlib.adapters.Adapter

/**
  * Database adapter trait
  *
  */
trait DBAdapter extends Adapter {
  //  Column   |          Type          | Collation | Nullable | Default
  // ----------+------------------------+-----------+----------+---------
  // sess_id   | character varying(128) |           | not null |
  // state     | integer                |           | not null |
  // type      | integer                |           | not null |
  // timestamp | bigint                 |           | not null |
  // task_id   | bigint                 |           | not null |

  val DB_TASKS_COL_SESS_ID_NAME = "sess_id"
  val DB_TASKS_COL_STATE_NAME = "state"
  val DB_TASKS_COL_TYPE_NAME = "type"
  val DB_TASKS_COL_TIMESTAMP_NAME = "timestamp"
  val DB_TASKS_COL_TASK_ID_NAME = "task_id"

  //  Column   |          Type          | Collation | Nullable | Default
  // ----------+------------------------+-----------+----------+---------
  // room_id   | bigint                 |           | not null |
  // room_type | integer                |           | not null |
  // room_name | character varying(100) |           | not null |
  // author_id | character varying(255) |           | not null |
  // clients   | text[]                 |           |          |
  val DB_ROOMS_COL_ROOM_ID_NAME = "room_id"
  val DB_ROOMS_COL_ROOM_TYPE_NAME = "room_type"
  val DB_ROOMS_COL_ROOM_NAME_NAME = "room_name"
  val DB_ROOMS_COL_AUTHOR_ID_NAME = "author_id"
  val DB_ROOMS_COL_CLIENTS_NAME = "clients"

  //  Column   |          Type          | Collation | Nullable | Default
  // ----------+------------------------+-----------+----------+---------
  // room_id   | bigint                 |           | not null |
  // author_id | character varying(255) |           | not null |
  // data      | character varying(255) |           | not null |
  val DB_MESSAGES_COL_ROOM_ID_NAME = "room_id"
  val DB_MESSAGES_COL_AUTHOR_ID_NAME = "author_id"
  val DB_MESSAGES_COL_DATA_NAME = "data"

  //   Column   |          Type          | Collation | Nullable | Default
  // ------------+-----------------------+-----------+----------+---------
  // client_id  | character varying(64)  |           | not null |
  // session_id | character varying(128) |           |          |
  val DB_SESSIONS_COL_CLIENT_ID_NAME = "client_id"
  val DB_SESSIONS_COL_SESSION_ID_NAME = "session_id"
}
