package com.redmadrobot.chatlib.adapters.db

object DBRequests {

  case class TaskRequest(sessionID: String, state: Int, taskType: Int, timeStamp: Long)
  case class RoomRequest(roomID: String, roomName: String, clientID: String)
  case class SessionRequest(clientID: String, sessionID: String)

}
