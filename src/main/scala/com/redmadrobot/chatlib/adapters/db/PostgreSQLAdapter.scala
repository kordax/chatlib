package com.redmadrobot.chatlib.adapters.db

import java.sql._

import com.redmadrobot.chatlib.adapters.db.PostgreSQLAdapter._
import com.redmadrobot.chatlib.common.TaskState.TaskState
import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import com.redmadrobot.chatlib.protobuf.messages.TaskMessage.{TaskCreateRoomMessage, TaskPostMessage}
import org.log4s.getLogger

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Simple PostgreSQL adapter.
  *
  * @param connectionString jdbc connection string
  */
class PostgreSQLAdapter(connectionString: String = s"jdbc:postgresql://localhost:5432/$DB_DEFAULT_DB_NAME?user=$DB_DEFAULT_DB_NAME") extends DBAdapter {
  private val LOGGER = getLogger

  private var connection = None: Option[Connection]
  private val validationTimeoutS = 5 // Seconds

  import java.sql.PreparedStatement

  var prepInsertTaskStatement: PreparedStatement = _
  var prepInsertSessionStatement: PreparedStatement = _
  var prepInsertRoomStatement: PreparedStatement = _
  var prepInsertToRoomStatement: PreparedStatement = _
  var prepInsertChatMessageStatement: PreparedStatement = _
  var prepRemoveFromRoomStatement: PreparedStatement = _
  var prepGetRoomStatement: PreparedStatement = _
  var prerGetRoomsStatement: PreparedStatement = _
  var prepGetRoomClientsStatement: PreparedStatement = _
  var prepGetLastRoomStatement: PreparedStatement = _
  var prepGetLastRoomIdStatement: PreparedStatement = _
  var prepGetClientSessionStatement: PreparedStatement = _
  var prepGetMessagesStatement: PreparedStatement = _

  val prepInsertTaskStatementStr: String = s"INSERT INTO $DB_TASKS_TABLE_NAME ($DB_TASKS_COL_TASK_ID_NAME, " +
    s"$DB_TASKS_COL_SESS_ID_NAME, " +
    s"$DB_TASKS_COL_STATE_NAME, " +
    s"$DB_TASKS_COL_TYPE_NAME, " +
    s"$DB_TASKS_COL_TIMESTAMP_NAME) VALUES " +
    s"""(0, ?, ?, ?, ?);"""
  val prepInsertSessionStatementStr: String = s"""INSERT INTO $DB_SESSIONS_TABLE_NAME ("$DB_SESSIONS_COL_CLIENT_ID_NAME", "$DB_SESSIONS_COL_SESSION_ID_NAME") VALUES (?, ?)
                                                 | ON CONFLICT("$DB_SESSIONS_COL_CLIENT_ID_NAME") DO UPDATE SET $DB_SESSIONS_COL_SESSION_ID_NAME=EXCLUDED.$DB_SESSIONS_COL_SESSION_ID_NAME
                                                 | RETURNING $DB_SESSIONS_COL_CLIENT_ID_NAME;""".stripMargin
  val prepInsertRoomStatementStr: String = s"INSERT INTO $DB_ROOMS_TABLE_NAME ($DB_ROOMS_COL_ROOM_ID_NAME, " +
    s"$DB_ROOMS_COL_ROOM_TYPE_NAME, " +
    s"$DB_ROOMS_COL_ROOM_NAME_NAME, " +
    s"$DB_ROOMS_COL_AUTHOR_ID_NAME) VALUES " +
    s"""(?, 1, ?, ?);"""
  val prepInsertToRoomStatementStr: String = s"UPDATE $DB_ROOMS_TABLE_NAME SET clients = array_append(clients, text(?)) WHERE room_id=?;"
  val prepInsertChatMessageStatementStr: String = s"INSERT INTO $DB_MESSAGES_TABLE_NAME ($DB_MESSAGES_COL_ROOM_ID_NAME, " +
    s"$DB_MESSAGES_COL_AUTHOR_ID_NAME, " +
    s"$DB_MESSAGES_COL_DATA_NAME) VALUES " +
    s"""(?, ?, ?);"""
  val prepRemoveFromRoomStatementStr: String = s"UPDATE $DB_ROOMS_TABLE_NAME SET clients = array_remove(clients, text(?)) WHERE room_id=?;"
  val prepGetRoomStatementStr: String = s"SELECT * FROM $DB_ROOMS_TABLE_NAME WHERE $DB_ROOMS_COL_ROOM_ID_NAME=?;"
  val prepGetRoomsStatementStr: String = s"SELECT $DB_ROOMS_COL_ROOM_ID_NAME, " +
    s"$DB_ROOMS_COL_ROOM_TYPE_NAME, " +
    s"$DB_ROOMS_COL_ROOM_NAME_NAME, " +
    s"$DB_ROOMS_COL_CLIENTS_NAME, " +
    s"$DB_ROOMS_COL_AUTHOR_ID_NAME FROM $DB_ROOMS_TABLE_NAME;"
  val prepGetRoomClientsStatementStr:String = s"SELECT $DB_ROOMS_COL_CLIENTS_NAME FROM $DB_ROOMS_TABLE_NAME " + s"WHERE $DB_ROOMS_COL_ROOM_ID_NAME=?;"
  val prepGetLastRoomStatementStr: String = s"SELECT t.$DB_ROOMS_COL_ROOM_ID_NAME, " +
    s"t.$DB_ROOMS_COL_ROOM_TYPE_NAME, " +
    s"t.$DB_ROOMS_COL_AUTHOR_ID_NAME, " +
    s"t.$DB_ROOMS_COL_CLIENTS_NAME " +
    s"FROM rooms as t INNER JOIN" +
    s"(SELECT max($DB_ROOMS_COL_ROOM_ID_NAME) as id FROM rooms) as j ON t.$DB_ROOMS_COL_ROOM_ID_NAME = j.id;"
  val prepGetLastRoomIdStatementStr: String = s"SELECT t.$DB_ROOMS_COL_ROOM_ID_NAME, " +
    s"t.$DB_ROOMS_COL_ROOM_TYPE_NAME, " +
    s"t.$DB_ROOMS_COL_AUTHOR_ID_NAME, " +
    s"t.$DB_ROOMS_COL_CLIENTS_NAME " +
    s"FROM rooms as t INNER JOIN" +
    s"(SELECT max($DB_ROOMS_COL_ROOM_ID_NAME) as id FROM rooms) as j ON t.$DB_ROOMS_COL_ROOM_ID_NAME = j.id;"
  val prepGetClientSessionStatementStr: String = "SELECT * FROM $DB_SESSIONS_TABLE_NAME WHERE $DB_SESSIONS_COL_CLIENT_ID_NAME=?;"
  val prepGetMessagesStatementStr: String = s"""SELECT $DB_MESSAGES_COL_ROOM_ID_NAME,
                                      |$DB_MESSAGES_COL_AUTHOR_ID_NAME,
                                      |$DB_MESSAGES_COL_DATA_NAME FROM $DB_MESSAGES_TABLE_NAME WHERE $DB_MESSAGES_COL_ROOM_ID_NAME=?;""".stripMargin

  /**
    * Connect method
    */
  def connect(): Unit = {
    connect(30000)
  }

  /**
    * Connect method
    *
    * @param timeoutMS timeout in milliseconds
    * @throws java.sql.SQLException if some error has occurred
    * @throws java.sql.SQLTimeoutException if connection timeout has occurred
    */
  @throws(classOf[SQLException])
  @throws(classOf[SQLTimeoutException])
  def connect(timeoutMS: Int): Unit = {
    if (isConnected) {
      LOGGER.error(s"Connection already established [$connectionString]")
    }

    LOGGER.info(s"Connecting to: $connectionString")
    try {
      DriverManager.setLoginTimeout(timeoutMS / 1000)
      connection = Some(DriverManager.getConnection(connectionString)) //jdbc:postgresql://postgresql-vm:5432/DB_NAME?user=DB_USER
    } catch {
      case e: SQLException =>
        LOGGER.error(s"Connection SQL error: ${e.getMessage}")
        throw e
      case et: SQLTimeoutException =>
        LOGGER.error(s"Connection timeout error: ${et.getMessage}")
        throw et
    }

    connection.get.setAutoCommit(true)

    prepInsertTaskStatement = connection.get.prepareStatement(prepInsertTaskStatementStr)
    prepInsertSessionStatement = connection.get.prepareStatement(prepInsertSessionStatementStr)
    prepInsertRoomStatement = connection.get.prepareStatement(prepInsertRoomStatementStr)
    prepInsertToRoomStatement = connection.get.prepareStatement(prepInsertToRoomStatementStr)
    prepInsertChatMessageStatement = connection.get.prepareStatement(prepInsertChatMessageStatementStr)
    prepRemoveFromRoomStatement = connection.get.prepareStatement(prepRemoveFromRoomStatementStr)
    prepGetRoomStatement = connection.get.prepareStatement(prepGetRoomStatementStr)
    prerGetRoomsStatement = connection.get.prepareStatement(prepGetRoomsStatementStr)
    prepGetRoomClientsStatement = connection.get.prepareStatement(prepGetRoomClientsStatementStr)
    prepGetLastRoomStatement = connection.get.prepareStatement(prepGetLastRoomStatementStr)
    prepGetLastRoomIdStatement = connection.get.prepareStatement(prepGetLastRoomIdStatementStr)
    prepGetClientSessionStatement = connection.get.prepareStatement(prepGetClientSessionStatementStr)
    prepGetMessagesStatement = connection.get.prepareStatement(prepGetMessagesStatementStr)
  }

  /**
    * Disconnect method. This method is used to clear up connection resources.
    */
  def disconnect(): Unit = {
    if (isConnected) {
      connection.get.close()
    } else {
      LOGGER.error(s"Failed to disconnect since there's no established connection [$connectionString]")
    }
  }

  /**
    * Determine whether adapter connection is active
    *
    * @return <code>true</code> if connection is valid, <code>false</code> otherwise
    */
  def isConnected: Boolean = {
    this.synchronized {
      if (connection.isDefined) {
        connection.get.isValid(validationTimeoutS)
      } else {
        false
      }
    }
  }

  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  override def getConnectionString: String = {
    connectionString
  }

  /**
    * Insert task into DB
    *
    * @param message message
    * @throws SQLException if SQL error occurred
    */
  @throws(classOf[SQLException])
  def insertTaskAudit(message: PBMessage, state: TaskState): Int = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // sess_id   | character varying(128) |           | not null |
    // state     | integer                |           | not null |
    // type      | integer                |           | not null |
    // timestamp | bigint                 |           | not null |
    // task_id   | bigint                 |           | not null |
    prepInsertTaskStatement.setString(1, message.clientName)
    prepInsertTaskStatement.setInt(2, state.id)
    prepInsertTaskStatement.setLong(3, message.`type`.index)
    prepInsertTaskStatement.setLong(4, message.timeStamp)

    LOGGER.debug(prepInsertTaskStatementStr)

    prepInsertTaskStatement.executeUpdate()
  }

  /**
    * Insert new group chat message into DB
    *
    * @param clientID client ID
    * @param message new group chat task
    * @throws SQLException if SQL error occurred
    */
  @throws(classOf[SQLException])
  def insertCreateRoomMessage(clientID: String, roomID: Long, message: TaskCreateRoomMessage): Long = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    prepInsertRoomStatement.setLong(1, roomID)
    prepInsertRoomStatement.setString(2, message.roomName)
    prepInsertRoomStatement.setString(3, clientID)

    LOGGER.debug(prepInsertRoomStatementStr)

    prepInsertRoomStatement.executeUpdate()
  }

  /**
    * Insert post message into DB
    *
    * @param roomID chat ID
    * @param clientID client ID
    * @param message post task
    * @throws SQLException if SQL error occurred
    * @return room id[
    */
  @throws(classOf[SQLException])
  def insertChatMessage(roomID: Long, clientID: String, message: TaskPostMessage): Boolean = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // author_id | character varying(255) |           | not null |
    // data      | character varying(255) |           | not null |
    prepInsertChatMessageStatement.setLong(1, roomID)
    prepInsertChatMessageStatement.setString(2, clientID)
    prepInsertChatMessageStatement.setString(3, message.data)

    LOGGER.debug(prepInsertChatMessageStatementStr)

    prepInsertChatMessageStatement.execute()
  }

  /**
    * Insert client name to a room
    *
    * @param roomID room ID
    * @param clientID client name
    * @throws java.sql.SQLException if SQL error occurred
    */
  @throws(classOf[SQLException])
  def insertClientToRoom(roomID: Long, clientID: String): Boolean = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    // clients   | text[]                 |           |          |
    prepInsertToRoomStatement.setString(1, clientID)
    prepInsertToRoomStatement.setLong(2, roomID)

    LOGGER.debug(prepInsertToRoomStatementStr)

    prepInsertToRoomStatement.execute()
  }

  /**
    * Update client session
    *
    * @param clientID client ID
    * @param sessionID session ID
    * @throws java.sql.SQLException if SQL error occurred
    */
  @throws(classOf[SQLException])
  def updateClientSession(clientID: String, sessionID: String): Boolean = synchronized {
    //                       Table "public.sessions"
    //   Column   |          Type          | Collation | Nullable | Default
    //------------+------------------------+-----------+----------+---------
    // client_id  | character varying(64)  |           | not null |
    // session_id | character varying(128) |           |          |
    prepInsertSessionStatement.setString(1, clientID)
    prepInsertSessionStatement.setString(2, sessionID)

    LOGGER.debug(prepInsertSessionStatementStr)

    prepInsertSessionStatement.execute()
  }

  /**
    * Remove client from room
    *
    * @param roomID room ID
    * @param clientID client ID
    * @throws java.sql.SQLException if SQL error occurred
    */
  @throws(classOf[SQLException])
  def removeClientFromRoom(roomID: Long, clientID: String): Boolean = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    // clients   | text[]                 |           |          |
    prepRemoveFromRoomStatement.setString(1, clientID)
    prepRemoveFromRoomStatement.setLong(2, roomID)

    LOGGER.debug(prepRemoveFromRoomStatementStr)

    prepRemoveFromRoomStatement.execute()
  }

  /**
    * Get client session ID
    *
    * @throws SQLException if SQL error occurred
    * @return session IDs or nothing, if session ID wasn't found
    */
  @throws(classOf[SQLException])
  def getClientSession(clientID: String): String = synchronized {
    //   Column   |          Type          | Collation | Nullable | Default
    // ------------+-----------------------+-----------+----------+---------
    // client_id  | character varying(64)  |           | not null |
    // session_id | character varying(128) |           |          |
    prepGetClientSessionStatement.setString(1, clientID)

    LOGGER.debug(prepGetClientSessionStatementStr)

    val resultSet = prepGetClientSessionStatement.executeQuery()

    if (resultSet.next()) {
      resultSet.getString(DB_SESSIONS_COL_SESSION_ID_NAME)
    } else {
      ""
    }
  }

  /**
    * Get messages for current room ID
    *
    * @param roomID room ID
    * @throws SQLException if SQL error occurred
    * @return result as [[ResultSet]] instance
    */
  @throws(classOf[SQLException])
  def getMessages(roomID: Long): ResultSet = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // author_id | character varying(255) |           | not null |
    // data      | character varying(255) |           | not null |
    prepGetMessagesStatement.setLong(1, roomID)

    LOGGER.debug(prepGetMessagesStatementStr)

    prepGetMessagesStatement.executeQuery()
  }

  /**
    * Get current rooms
    *
    * @throws SQLException if SQL error occurred
    * @return result as [[ResultSet]] instance
    */
  @throws(classOf[SQLException])
  def getRoom(roomID: Long): ResultSet = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    //﻿clients   | text[]                 |           |          |
    LOGGER.debug(prepGetRoomStatementStr)

    prepGetRoomStatement.executeQuery()
  }

  /**
    * Get room with highest room_id
    *
    * @throws java.sql.SQLException if SQL error occurred
    * @return result as [[ResultSet]] instance
    */
  @throws(classOf[SQLException])
  def getLastRoom: ResultSet = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    //﻿clients   | text[]                 |           |          |
    LOGGER.debug(prepGetLastRoomStatementStr)

    prepGetLastRoomStatement.executeQuery()
  }

  /**
    * Get room id of a room with max room_id
    *
    * @throws java.sql.SQLException if SQL error occurred
    * @return last room id or -1 if nothing was found
    */
  @throws(classOf[SQLException])
  def getLastRoomID: Long = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    //﻿clients   | text[]                 |           |          |
    LOGGER.debug(prepGetLastRoomIdStatementStr)

    val resultSet = prepGetLastRoomIdStatement.executeQuery()

    if (resultSet.next()) {
      resultSet.getLong(DB_ROOMS_COL_ROOM_ID_NAME)
    } else {
      -1
    }
  }

  /**
    * Get current rooms
    *
    * @throws SQLException if SQL error occurred
    * @return result as [[ResultSet]] instance
    */
  @throws(classOf[SQLException])
  def getRooms: ResultSet = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    //﻿clients   | text[]                 |           |          |
    LOGGER.debug(prepGetRoomsStatementStr)

    prerGetRoomsStatement.executeQuery()
  }

  /**
    * Get array of clients for room
    *
    * @param roomID room ID
    * @throws java.sql.SQLException if SQL error occurred
    * @return
    */
  @throws(classOf[SQLException])
  def getRoomClients(roomID: Long): scala.Array[String] = synchronized {
    //  Column   |          Type          | Collation | Nullable | Default
    // ----------+------------------------+-----------+----------+---------
    // room_id   | bigint                 |           | not null |
    // room_type | integer                |           | not null |
    // room_name | character varying(100) |           | not null |
    // author_id | character varying(255) |           | not null |
    //﻿clients   | text[]                 |           |          |
    prepGetRoomClientsStatement.setLong(1, roomID)

    LOGGER.debug(prepGetRoomClientsStatementStr)

    val resultSet = prepGetRoomClientsStatement.executeQuery()

    if (resultSet.next()) {
      val array = Option(resultSet.getArray(DB_ROOMS_COL_CLIENTS_NAME))

      if (array.isDefined) {
        resultSet.getArray(DB_ROOMS_COL_CLIENTS_NAME).getArray.asInstanceOf[scala.Array[String]]
      } else {
        new scala.Array[String](0)
      }
    } else {
      new scala.Array[String](0)
    }
  }
}

object PostgreSQLAdapter {
  val DB_DEFAULT_DB_NAME = "postgres"
  val DB_TASKS_TABLE_NAME = "tasks"
  val DB_ROOMS_TABLE_NAME = "rooms"
  val DB_MESSAGES_TABLE_NAME = "messages"
  val DB_SESSIONS_TABLE_NAME = "sessions"
}