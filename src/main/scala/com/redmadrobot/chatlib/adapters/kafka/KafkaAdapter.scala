package com.redmadrobot.chatlib.adapters.kafka

import java.util.Properties

import com.redmadrobot.chatlib.adapters.Adapter
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.common.serialization.{Deserializer, Serializer}

/**
  * Simple kafka producer and consumer wrapper
  *
  * @param bootstrapServers bootstrap server string
  * @param prodProperties   optional producer properties
  * @param consProperties   optional consumer properties
  * @tparam Key record key type
  * @tparam Val record value type
  * @param keyDeserializer
  * @param consumerGroupID
  * @param valueDeserializer
  * @param valueSerializer
  * @param keySerializer
  */
class KafkaAdapter[Key, Val](bootstrapServers: String,
                             consumerGroupID: String,
                             keySerializer: Serializer[Key],
                             valueSerializer: Serializer[Val],
                             keyDeserializer: Deserializer[Key],
                             valueDeserializer: Deserializer[Val],
                             prodProperties: Properties = new Properties(),
                             consProperties: Properties = new Properties()
                            ) extends Adapter {
  {
    prodProperties.put("bootstrap.servers", bootstrapServers)
    consProperties.put("bootstrap.servers", bootstrapServers)
    consProperties.put("group.id", consumerGroupID)
  }

  private val producer = new KafkaProducer[Key, Val](prodProperties, keySerializer, valueSerializer)
  private val consumer = new KafkaConsumer[Key, Val](consProperties, keyDeserializer, valueDeserializer)

  /**
    * Get producer
    *
    * @return [[KafkaProducer]] instance
    */
  def getProducer: org.apache.kafka.clients.producer.KafkaProducer[Key, Val] = {
    producer
  }

  /**
    * Get consumer
    *
    * @return [[KafkaConsumer]] instance
    */
  def getConsumer: org.apache.kafka.clients.consumer.KafkaConsumer[Key, Val] = {
    consumer
  }

  /**
    * Get consumer group id
    *
    * @return consumer group id
    */
  def getConsumerGroupID: String = {
    consumerGroupID
  }

  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  override def getConnectionString: String = {
    bootstrapServers
  }

  /**
    * Create new producer instance with same parameters
    *
    */
  def makeProducer(): KafkaProducer[Key, Val] = {
    new KafkaProducer[Key, Val](prodProperties, keySerializer, valueSerializer)
  }

  /**
    * Create new consumer instance with same parameters
    *
    */
  def makeConsumer(): KafkaConsumer[Key, Val] = {
    new KafkaConsumer[Key, Val](consProperties, keyDeserializer, valueDeserializer)
  }
}

object KafkaAdapter {
  val KAFKA_RESPONSES_TOPIC = "topic_responses"
  val KAFKA_SESSIONS_TOPIC = "topic_sessions"
  val KAFKA_TASKS_TOPIC = "TasksTopic"
  val KAFKA_USER_TOPIC_PREFIX = "UserTopic"
}