package com.redmadrobot.chatlib.adapters.kafka

import java.util

import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import org.apache.kafka.common.serialization.Deserializer
import org.log4s.getLogger

/**
  * ProtoBuf messages deserializer for KafkaAdapter
  *
  */
class PBMessageDeserializer extends Deserializer[PBMessage] {
  private val LOGGER = getLogger
  /**
    * Configure this class.
    *
    * @param configs configs in key/value pairs
    * @param isKey   whether is for key or value
    */
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {
    LOGGER.debug("Configuring deserializer")
  }

  /**
    * Deserialize a record value from a byte array into a value or object.
    *
    * @param topic topic associated with the data
    * @param data  serialized bytes; may be null; implementations are recommended to handle null by returning a value or null rather than throwing an exception.
    * @return deserialized typed data; may be null
    */
  override def deserialize(topic: String, data: Array[Byte]): PBMessage = {
    LOGGER.debug(s"""Serializing message for topic "$topic"""")
    PBMessage.parseFrom(data)
  }

  /**
    * Close this deserializer.
    *
    * This method must be idempotent as it may be called multiple times.
    */
  override def close(): Unit = {
    // There's nothing to do on close for this serializer
  }
}