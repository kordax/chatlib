package com.redmadrobot.chatlib.adapters.kafka

import java.util

import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import org.apache.kafka.common.serialization.Serializer
import org.log4s.getLogger

/**
  * ProtoBuf messages deserializer for KafkaAdapter
  *
  */
class PBMessageSerializer extends Serializer[PBMessage] {
  private val LOGGER = getLogger

  /**
    * Configure this class.
    *
    * @param configs configs in key/value pairs
    * @param isKey whether is for key or value
    */
  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {
    LOGGER.debug("Configuring deserializer")
  }

  /**
    * Convert `data` into a byte array.
    *
    * @param topic topic associated with data
    * @param data typed data
    * @return serialized bytes
    */
  override def serialize(topic: String, data: PBMessage): Array[Byte] = {
    LOGGER.debug(s"""Serializing message for topic "$topic"""")
    data.toByteArray
  }

  /**
    * Close this serializer.
    *
    * This method must be idempotent as it may be called multiple times.
    */
  override def close(): Unit = {
    // There's nothing to do on close for this serializer
  }
}