package com.redmadrobot.chatlib.adapters.mq

import java.io.IOException

import akka.actor.{ActorRef, ActorSystem}
import com.newmotion.akka.rabbitmq.{ChannelActor, ConnectionActor, CreateChannel}
import com.rabbitmq.client.{BuiltinExchangeType, Channel, Consumer, MetricsCollector}
import com.redmadrobot.chatlib.adapters.Adapter
import com.redmadrobot.chatlib.common.Constants
import javax.net.ssl.SSLContext
import org.log4s._

import scala.concurrent.TimeoutException

/**
  * Simple RabbitMQ adapter with SSL support.
  * Default constructor initiates SSL connection to remote RabbitMQ listener socket using AMQP protocol.
  *
  * @param connectionFactory [[com.newmotion.akka.rabbitmq.ConnectionFactory]] instance
  */
class RabbitMQAdapter(connectionFactory: com.newmotion.akka.rabbitmq.ConnectionFactory) extends Adapter {
  private val LOGGER = getLogger
  /** Default akka actor system */
  private implicit val system: akka.actor.ActorSystem = ActorSystem()
  /** Current [[com.newmotion.akka.rabbitmq.Connection]] instance */
  private var connection = None: Option[com.newmotion.akka.rabbitmq.Connection]
  /** We use a single-channel mode only, since we don't need another channel ATM */
  var connChannel: Option[Channel] = None: Option[Channel]
  /** Current connection actor ref */
  var actor: ActorRef = _
  /** We need to attach at least one actor for reconnection logic */
  actor = system.actorOf(ConnectionActor.props(connectionFactory), Constants.RABBIT_MQ_ACTOR_PREFIX + connectionFactory.getHost + ":" + connectionFactory.getPort)

  private var numOfPubs = 0L
  private var numOfSubs = 0L

  /**
    * Connection factory preconfiguration
    */
  /** Always use automatic recovery for connection */
  connectionFactory.setAutomaticRecoveryEnabled(true)
  /** Set connection timeout */
  connectionFactory.setConnectionTimeout(15)

  /**
    * Constructor with default [[com.newmotion.akka.rabbitmq.ConnectionFactory]]
    *
    * @param host remote host
    * @param port remote port
    */
  def this(host: String, port: Int) {
    this(new com.newmotion.akka.rabbitmq.ConnectionFactory())
    connectionFactory.setHost(host)
    connectionFactory.setPort(port)
  }

  /**
    * Constructor with default [[com.newmotion.akka.rabbitmq.ConnectionFactory]]
    *
    * @param host remote host
    * @param port remote port
    * @param username remote username
    * @param password remote password
    */
  def this(host: String, port: Int, username: String, password: String) {
    this(new com.newmotion.akka.rabbitmq.ConnectionFactory())
    connectionFactory.setHost(host)
    connectionFactory.setPort(port)
    connectionFactory.setUsername(username)
    connectionFactory.setPassword(password)
  }

  /**
    * Constructor with default [[com.newmotion.akka.rabbitmq.ConnectionFactory]] and custom [[SSLContext]]
    *
    * @param host remote host
    * @param port remote port
    * @param username remote username
    * @param password remote password
    * @param context [[SSLContext]] instance
    */
  def this(host: String, port: Int, username: String, password: String, context: SSLContext) {
    this(new com.newmotion.akka.rabbitmq.ConnectionFactory())
    connectionFactory.setHost(host)
    connectionFactory.setPort(port)
    connectionFactory.setUsername(username)
    connectionFactory.setPassword(password)
    connectionFactory.useSslProtocol(context)
  }

  /**
    * Initiate connection and register publisher
    *
    * @throws IOException if any error occurs
    * @throws TimeoutException if connection timeout occurs
    */
  @throws(classOf[IOException])
  @throws(classOf[TimeoutException])
  def connect(): Unit = {
    connect(30000)
  }

  /**
    * Initiate connection and register publisher
    *
    * @param timeoutMS timeout in milliseconds
    * @throws IOException if any error occurs
    * @throws TimeoutException if connection timeout occurs
    */
  @throws(classOf[IOException])
  @throws(classOf[TimeoutException])
  def connect(timeoutMS: Int): Unit = {
    if (connection.isEmpty) {
      LOGGER.info(s"Connecting to ${connectionFactory.getHost}:${connectionFactory.getPort}")
      try {
        connectionFactory.setConnectionTimeout(timeoutMS.toInt)
        connection = Some(connectionFactory.newConnection())
        connChannel = Some(connection.get.createChannel())
      } catch {
        case e: IOException =>
          LOGGER.error(s"Connection error: ${e.getMessage}")
          throw e
        case er: TimeoutException =>
          LOGGER.error(s"Connection timeout to ${connectionFactory.getHost}:${connectionFactory.getPort}:${er.getMessage}")
          throw er
      }
    } else {
      LOGGER.debug("Adapter is already connected")
    }
  }

  /**
    * Close connection
    */
  def disconnect(): Unit = {
    LOGGER.info("Disconnecting: " + connectionFactory)

    if (connection.isDefined) {
      connection.get.close()
    }

    connection = None
  }

  /**
    * Close connection
    *
    * @param timeout timeout to close connection
    */
  def disconnect(timeout: Int): Unit = {
    if (connection.isDefined) {
      connection.get.close(timeout)
    }
  }

  /**
    * Determine whether adapter connection is active
    *
    * @return <code>true</code> if connection is valid, <code>false</code> otherwise
    */
  def isConnected: Boolean = {
    if (connection.isDefined) {
      if (connChannel.isDefined) {
        connChannel.get.isOpen
      } else {
        false
      }
    } else {
      false
    }
  }

  /**
    * Get connection string (host, port or a full URL)
    *
    * @return host string that might have a port, or a full url
    */
  override def getConnectionString: String = {
    s"${connectionFactory.getHost}:${connectionFactory.getPort}"
  }

  /**
    * Set custom metrics collector
    *
    * @param metricsCollector [[com.rabbitmq.client.MetricsCollector]] instance
    */
  def setMetricsCollector(metricsCollector: MetricsCollector): Unit = {
    connectionFactory.setMetricsCollector(metricsCollector)
  }

  /**
    * Publish message bytes
    *
    * @param bytes message bytes
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  def publish(bytes: Array[Byte]): Unit = {
    LOGGER.debug(s"Publishing to ${RabbitMQAdapter.MQ_TASKS_EXCHANGE}, ${bytes.length} bytes")

    if (!isConnected)
      throw new IOException("Failed to publish, connection isn't open")

    connChannel.get.basicPublish(RabbitMQAdapter.MQ_TASKS_EXCHANGE, RabbitMQAdapter.MQ_TASKS_ROUTING_KEY, null, bytes)
  }

  /**
    * Publish message bytes
    *
    * @param exchange exchange
    * @param bytes message bytes
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  def publish(bytes: Array[Byte],
              exchange: String = RabbitMQAdapter.MQ_RESPONSE_EXCHANGE,
              routingKey: String = RabbitMQAdapter.MQ_RESPONSE_ROUTING_KEY): Unit = {
    LOGGER.debug(s"Publishing to $exchange, ${bytes.length} bytes")

    if (!isConnected)
      throw new IOException("Failed to publish, connection isn't open")

    connChannel.get.basicPublish(exchange, routingKey, true, false, null, bytes)
  }

  /**
    * Subscribe to messages on remote RabbitMQ node
    *
    * @param consumer provided consumer callback function of [[Consumer]] type
    * @param queueName queue name
    * @param exchange exchange
    * @param routingKey routing key
    * @return consumer unique tag
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  def subscribe(consumer: Consumer,
                queueName: String = RabbitMQAdapter.MQ_TASKS_QUEUE_NAME,
                exchange: String = RabbitMQAdapter.MQ_TASKS_EXCHANGE,
                routingKey: String = RabbitMQAdapter.MQ_TASKS_ROUTING_KEY): String = {
    LOGGER.debug(s"Subscribing to $queueName, $exchange, $routingKey")

    if (!isConnected)
      throw new IOException("Failed to subscribe, connection isn't open")

    registerSubscriber(queueName, exchange, routingKey)
    connChannel.get.basicConsume(queueName, false, consumer)
  }

  /**
    * Unsubscribe consumer
    *
    * @param consumerTag consumer tag
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  def unsubscribe(consumerTag: String): Unit = {
    LOGGER.debug(s"Unsubscribing consumer $consumerTag")
    numOfSubs -= 1
    connChannel.get.basicCancel(consumerTag)
  }

  /**
    * Register WSProxy publisher
    *
    * @param queueName queue name
    * @param exchange exchange
    * @param routingKey routing key
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  private def registerPublisher(queueName: String = RabbitMQAdapter.MQ_TASKS_QUEUE_NAME,
                                exchange: String = RabbitMQAdapter.MQ_TASKS_EXCHANGE,
                                routingKey: String = RabbitMQAdapter.MQ_TASKS_ROUTING_KEY
                               ): String = {
    LOGGER.debug("Registering publisher")

    if (!isConnected)
      throw new IOException("Failed to register publisher, connection isn't open")

    connChannel = Some(connection.get.createChannel())

    var queue = connChannel.get.queueDeclare(queueName, false, true, true, null).getQueue
    connChannel.get.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT)
    connChannel.get.queueBind(queue, exchange, routingKey)

    numOfPubs += 1
    actor ! CreateChannel(ChannelActor.props(), Some(s"Publisher_${connectionFactory.getHost}:${connectionFactory.getPort}_$queueName-$numOfPubs"))
    queue
  }

  /**
    * Register WSProxy publisher
    *
    * @param queueName queue name
    * @param exchange exchange
    * @param routingKey routing key
    * @throws IOException if any error occurs
    */
  @throws(classOf[IOException])
  private def registerSubscriber(queueName: String = RabbitMQAdapter.MQ_TASKS_QUEUE_NAME,
                                 exchange: String = RabbitMQAdapter.MQ_TASKS_EXCHANGE,
                                 routingKey: String = RabbitMQAdapter.MQ_TASKS_ROUTING_KEY
                                ): String = {
    LOGGER.debug("Registering subscriber")

    if (!isConnected)
      throw new IOException("Failed to register subscriber, connection isn't open")

    var queue = connChannel.get.queueDeclare(queueName, false, true, true, null).getQueue
    connChannel.get.exchangeDeclare(exchange, BuiltinExchangeType.FANOUT)
    connChannel.get.queueBind(queue, exchange, routingKey)

    numOfSubs += 1
    actor ! CreateChannel(ChannelActor.props(), Some(s"Subscriber_${connectionFactory.getHost}:${connectionFactory.getPort}_$queueName-$numOfSubs"))
    queue
  }

  override def toString = s"RabbitMQAdapter($system, $connection, $connChannel, $actor)"
}

object RabbitMQAdapter {
  val MQ_TASKS_QUEUE_NAME = "TasksQueue"
  val MQ_TASKS_EXCHANGE = "TasksEx"
  val MQ_TASKS_ROUTING_KEY = "TasksRoutingKey"
  val MQ_RESPONSE_QUEUE_NAME_PREFIX = "ResponseQueue_"
  val MQ_RESPONSE_EXCHANGE = "ResponseEx"
  val MQ_RESPONSE_ROUTING_KEY = "ResponseRoutingKey"
}