package com.redmadrobot.chatlib.common

object Constants {
  val WSPROXY_APP_NAME = "WebSocket Proxy"
  val BACKEND_APP_NAME = "Backend"
  val WSPROXY_VERSION = "0.2"
  val WSPROXY_WEBSOCKET_PATH_SUFFIX = "ws"
  val RABBIT_MQ_ACTOR_PREFIX = "RabbitMQConnActor_"

  val WEB_SOCKET_NONSECURE_PROTOCOL = "ws"
  val WEB_SOCKET_SECURE_PROTOCOL = "wss"
}
