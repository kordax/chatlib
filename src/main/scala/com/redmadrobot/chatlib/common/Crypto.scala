package com.redmadrobot.chatlib.common

import java.math.BigInteger
import java.security.{MessageDigest, SecureRandom}

object Crypto {
  /**
    * Generate pseudorandom session id using [[SecureRandom]] of SHA1PRNG type and SHA-256 digest
    *
    * @param bytes bytes for digest
    * @throws java.lang.Exception if any error occurs
    * @return hash as HEX string of SHA256 bytes
    */
  @throws(classOf[Exception])
  def generateSessionID(bytes: Array[Byte]): String = {
    val random = SecureRandom.getInstance("SHA1PRNG")
    val digest = MessageDigest.getInstance("SHA-256")
    val bytes = digest.digest(random.nextLong().toString.getBytes())

    String.format("%064x", new BigInteger(1, bytes))
  }
}
