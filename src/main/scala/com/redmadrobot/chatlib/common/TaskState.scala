package com.redmadrobot.chatlib.common

/**
  * Messages types enumeration
  */
object TaskState extends Enumeration {
  type TaskState = Value
  val RECEIVED: TaskState.Value = Value(1, "state_received")
  val IN_PROGRESS: TaskState.Value = Value(2, "state_in_progress")
  val COMPLETED: TaskState.Value = Value(3, "state_completed")
}