package com.redmadrobot.chatlib.logging

import java.io.{PrintWriter, StringWriter}

object LoggingHelpers {
  /**
    * Get exception stack trace as a [[String]]
    *
    * @param t throwable
    * @return stack trace as a [[String]]
    */
  def getStackTraceAsString(t: Throwable): String = {
    val sw = new StringWriter
    t.printStackTrace(new PrintWriter(sw))
    sw.toString
  }
}
